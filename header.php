<!-- --------------------------- PERFIL ------------------------------ -->
<?php
$sql = "SELECT * FROM perfil WHERE idPerfil = 1";
$perfil = $mysqli->query($sql);
$row = $perfil->fetch_array(MYSQLI_ASSOC);
?>
<header id="header">
    <div class="col-12 row m-0">
        <div class="col-sm col-md-3 my-3">
            <img src="./images/<?php echo $row['imagen'] . '" alt="' . $row['nombres']; ?>" class="img-responsive col rounded-top" />
        </div>
        <div class="col-sd col-md-9 row-md column-sm my-3">
            <div class="col-12">
                <h2 class="display-md-4 text-sm-left text-md-right"><?php echo $row['nombres'] . ' ' . $row['apellido1'] . ' ' . $row['apellido2']; ?></h2>
                <p class="text-sm-left text-md-right"><?php echo $row['titulo']; ?></p>
            </div>
            <div class="col-12 mb-0 text-justify">
                <p><?php echo $row['descripcion'];
                    $perfil->free_result(); ?></p>
            </div>
        </div>
    </div>
    <hr>
</header>