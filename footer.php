<?php
$sql = "SELECT * FROM perfil WHERE idPerfil = 1";
$perfil = $mysqli->query($sql);
$row = $perfil->fetch_array(MYSQLI_ASSOC);
?>
<footer id="footer" class="col-12 row mt-3 py-3 m-0 flag">

    <h3 class="col-12 text-center">Contacto</h3>

    <div class="col-md-4 col-sm-12">
        <p class="text-md-left text-center">Movil:</p>
        <p class="text-md-left text-center"><?php echo $row['telefono']; ?></p>
    </div>

    <div class="col-md-4 col-sm-12">
        <p class="text-center">Email:</p>
        <p class="text-center">
            <?php echo $row['email'];
            $perfil->free_result();
            ?>
        </p>
    </div>

    <div class="col-md-4 col-sm-12">
        <p class="text-md-right text-center">Ciudad:</p>
        <p class="text-md-right text-center">Medellín / Colombia</p>
    </div>

</footer>