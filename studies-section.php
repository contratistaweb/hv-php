<?php
$sql = "SELECT * FROM studies WHERE idPerfil = 1";
$estudies = $mysqli->query($sql);

$array = array();
while ($row = mysqli_fetch_assoc($estudies)) {
    $array[] = $row;
}

?>
<section id="studies">
    <h1 class="text-center">Estudios</h1>
    <hr>
    <?php
    $i = 0;
    while ($i < count($array)) {
        echo '<div class="row py-3 my-2 mx-2"><div class="col-md-3 p-3 col-xs order-2"><p>fecha finalizacion: ';
        if($array[$i]['endDate'] === null){
            echo 'Actualmente</p>';
        }else{
            echo $array[$i]['endDate'].'</p>';
        }
        echo '<p>fecha de inicio: ' . $array[$i]['startDate'] . '</p></div> <div class="col-md-9 col-xs order-md-2 shadow p-3"><h3>' . $array[$i]['obtainedTitle'] .' - '.$array[$i]['educationLevel']. '</h3><h5>' . $array[$i]['institute'] . '</h5><p>' . $array[$i]['descStudy'] . '</p></div></div>';
        $i++;
    }
    $estudies->free_result();
    ?>
    <hr>
</section>