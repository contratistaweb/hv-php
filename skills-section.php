    <?php
    $sql = "SELECT lang, levelLang FROM skills WHERE idPerfil = 1";
    $skills = $mysqli->query($sql);

    $array = array();
    while ($row = mysqli_fetch_assoc($skills)) {
        $array[] = $row;
    }
    ?>
    <section id="skills" class="row m-0 my-3">
        <hr>
        <h2 class="text-center col-12">Habilidades</h2>
        <div class="col-12 d-flex flex-wrap justify-content-around">
            <?php
            $i = 0;
            while ($i < count($array)) {
                echo ('<div class="flag shadow col-md-2 m-1"><h6 class="col">' . $array[$i]['lang'] . '</h6><p class="col">Nivel: ' . $array[$i]['levelLang'] . '</p></div>');
                $i++;
            }
            $skills->free_result();
            ?>
        </div>

    <hr>

    </section>