<?php
$sql = "SELECT * FROM experiences WHERE idPerfil = 1";
$experiences = $mysqli->query($sql);

$array = array();
while ($row = mysqli_fetch_assoc($experiences)) {
    $array[] = $row;
}

?>
<section id="experiences">
    <h1 class="text-center">Experiencia</h1>
    <hr>
    <?php
    $i = 0;
    while ($i < count($array)) {
        echo '<div class="d-flex flex-column flex-md-row py-3 my-2 mx-2"><div class="col-sm col-md-3 col-xs p-3 order-2"><p>fecha finalizacion: ';
        if ($array[$i]['endDate'] === null) {
            echo 'Actualmente';
        } else {
            echo $array[$i]['endDate'];
        }
        echo '</p><p>fecha de inicio: ' . $array[$i]['startDate'] . '</p></div> <div class="col col-md-9 shadow p-3 order-md-2"><h3>' . $array[$i]['position'] . '</h3><h5>' . $array[$i]['company'] . '</h5><p>' . $array[$i]['descPosition'] . '</p></div></div>';
        $i++;
    }
    $experiences->free_result();
    ?>
    <hr>
</section>